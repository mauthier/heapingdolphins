---
title: "PriorElicitation"
author: Matthieu Authier, Isabelle Albert, Daouda Ba, Sophie Donnet, Mathieu Genu,
  Eric Parent
date: "02/10/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Prior Elicitation

We illustrate how an informative prior for parameters controling the heaping process can be recovered from the answers from an expert, and fed into **Stan**.

```{r functions}
source("functions/functionsElicitation.R")
source("functions/plotElicited2.r")

```

## Elicitation from the expert

We have answers from several expert stored in a table : 
```{r multi_expert_table}
multi_expert_table <- read.table("data/2_Resultats_Shiny.csv", 
                                sep = ";",
                                h=T) %>% 
  mutate(lastCurve = ifelse(is.na(Q50_inf), FALSE, TRUE))

multi_expert_table %>% 
  DT::datatable()

```

```{r transform_data}

tmp <- multi_expert_table %>% 
  mutate(expert = paste(Nom,Prénom,sep="_")) %>% 
  mutate(Q_inf = ifelse(is.na(Q10_inf), Q50_inf, Q10_inf),
         Q_sup = ifelse(is.na(Q10_sup), Q50_sup, Q10_sup),
         Q_P = ifelse(is.na(Q10_P), Q50_P, Q10_P),
         Q_Conf = ifelse(is.na(Q10_Conf), Q50_Conf, Q10_Conf)) %>% 
  select(-(Q10_inf:Q50_Conf))

tau0 <- tmp %>% 
  select(starts_with("tau_0"), expert) %>% 
  mutate(param = "tau_0") 
colnames(tau0)[1:4] <- c("xinf","xsup","P","Conf")

tau1 <- tmp %>% 
  select(starts_with("tau_1"), expert) %>% 
  mutate(param = "tau_1")
colnames(tau1)[1:4] <- c("xinf","xsup","P","Conf")


X1_1 <- tmp %>% 
  select(starts_with("X1_1"), expert) %>% 
  mutate(param = "1_1")
colnames(X1_1)[1:4] <- c("xinf","xsup","P","Conf")


X1_2 <- tmp %>% 
  select(starts_with("X1_2"), expert) %>% 
  mutate(param = "1_2")
colnames(X1_2)[1:4] <- c("xinf","xsup","P","Conf")

  
Q <- tmp %>% 
  select(starts_with("Q"), expert) %>% 
  mutate(param = "Q")
colnames(Q)[1:4] <- c("xinf","xsup","P","Conf")


df_elicited_expert <- bind_rows(tau0,tau1,X1_1,X1_2,Q) %>%
  left_join(tmp %>% 
              select(expert, lastCurve), 
            by = "expert")
  


```



From these answers, a call to the function _sampleElicitedValue_ will return draws from the answers with uncertainty. The function _inverseConstraints_ maps these answers to a prior, which can be visualized with functions _plotElicited_ and _plotElicited2_:

```{r plot_by_expert}

expert_name <- unique(df_elicited_expert$expert) 

list_echan_elicited <- list()
list_echanParamElicited <- list()

for(e in 1:length(expert_name)) {
  
  lastCurve <- df_elicited_expert$lastCurve[df_elicited_expert$expert %in% expert_name[e]] %>% unique()
  
  expert_elicited <- df_elicited_expert %>% 
    filter(expert == expert_name[e]) 
  row.names(expert_elicited) <- expert_elicited$param
  expert_elicited <- expert_elicited %>%
    select(-param, -expert, -lastCurve)
    
  
  # sampling elicited values with uncertainty
  echan_elicited <- sampleElicitedValue(1e5, expert_elicited)
  # transform into parameters
  echanParamElicited <- inverseConstraints(
    expert_elicited[,3],
    echan_elicited,
    lastCurve = lastCurve
    )
  # plot curves with uncertainty
  plotElicited2(echanParamElicited, 
                expert_elicited, 
                plotOptions = list(xmax = 250, heaping_levels = c(5, 10, 50)),
                lastCurve = lastCurve
  ) +
    labs(title = expert_name[e])
  
  list_echan_elicited[[e]] <- echan_elicited
  list_echanParamElicited[[e]] <- echanParamElicited
  
}


```

The unconditional curves can also be recovered:

```{r}

dd_list <- list()
df_list <- list()

for(e in 1:length(expert_name)) {
  
  dd <- probCondRcppVect(
    X = 1:400,
    param_heaping = list(tau_0 = list_echanParamElicited[[e]]$tau_0,
                         lambda_0 = list_echanParamElicited[[e]]$lambda_0,
                         gamma = list_echanParamElicited[[e]]$gamma
                         )
    )
  param_name <- c("no_heaping", "round to 5", "round to 10", "round to 50")
  df <- NULL
  for(j in 1:length(param_name)) {
    df <- rbind(df,
                data.frame(true_count = 1:400,
                           y = apply(dd[, , j] * (1 - ifelse(j != 1, 1, 0) * dd[, , 1]), 2, mean),
                           lower = as.numeric(apply(dd[, , j] * (1 - ifelse(j != 1, 1, 0) * dd[, , 1]), 2, function(x) { quantile(x, probs = 0.1) })),
                           upper = as.numeric(apply(dd[, , j] * (1 - ifelse(j != 1, 1, 0) * dd[, , 1]), 2, function(x) { quantile(x, probs = 0.9) })),
                           behaviour = rep(param_name[j], 400)
                )
    )
  }; rm(j)
  df$what <- "elicited_prior"
  
  dd_list[[e]] <- dd
  df_list[[e]] <- df
}

```

For visualisation of the curves for the first expert:

```{r}
theme_set(theme_bw(base_size = 16))
df_list[[1]] %>% 
  ggplot(aes(x = true_count, y = y, ymin = lower, ymax = upper, fill = behaviour, color = behaviour)) + 
  geom_ribbon(alpha = 0.3) +
  geom_line() +
  scale_x_sqrt(name = "True count", breaks = c(1, 10, 20, 50, 100, 200, 300, 400)) +
  scale_y_continuous(name = "Probability", breaks = seq(0, 1, 0.1)) +
  facet_wrap(~ behaviour, ncol = 2) +
  scale_fill_brewer(name = "Heaping Behaviour: ", type = "qual", palette = "Set2") +
  scale_color_brewer(name = "Heaping Behaviour: ", type = "qual", palette = "Set2") +
  guides(color = "none") +
  coord_cartesian(xlim = c(1, 300)) +
  theme(legend.position = "top", 
        plot.title = element_text(lineheight = 0.8, face = "bold"),  
        axis.text.y = element_text(size = 12),
        axis.text.x = element_text(size = 12),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
        )
```

Or the fifth one:

```{r}
theme_set(theme_bw(base_size = 16))
df_list[[5]] %>% 
  ggplot(aes(x = true_count, y = y, ymin = lower, ymax = upper, fill = behaviour, color = behaviour)) + 
  geom_ribbon(alpha = 0.3) +
  geom_line() +
  scale_x_sqrt(name = "True count", breaks = c(1, 10, 20, 50, 100, 200, 300, 400)) +
  scale_y_continuous(name = "Probability", breaks = seq(0, 1, 0.1)) +
  facet_wrap(~ behaviour, ncol = 2) +
  scale_fill_brewer(name = "Heaping Behaviour: ", type = "qual", palette = "Set2") +
  scale_color_brewer(name = "Heaping Behaviour: ", type = "qual", palette = "Set2") +
  guides(color = "none") +
  coord_cartesian(xlim = c(1, 300)) +
  theme(legend.position = "top", 
        plot.title = element_text(lineheight = 0.8, face = "bold"),  
        axis.text.y = element_text(size = 12),
        axis.text.x = element_text(size = 12),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
        )
```

The object _echanParamElicited_ stores the draws from the prior. However, this corresponds to heaping parameters $\gamma$, and a different parametrization is needed for **Stan** (see main text). Constrained parameters $\gamma$ can be map to unconstrained parameters $\delta$ with the function _fromGammaToDelta_ (and the reverse operation is done with _fromDeltaToGamma_):

```{r}
list_delta_mat <- list()

for(e in 1:length(expert_name)) {
  
  delta_mat <- t(apply(list_echanParamElicited[[e]]$gamma, 1, fromGammaToDelta))
  
  list_delta_mat[[e]] <- delta_mat
  
}


```

A multivariate prior for the heaping parameters is thus:

```{r}
list_my_prior <- list()
sum_stat_prior <- list()

for(e in 1:length(expert_name)) {
  
  mv_prior <- with(list_echanParamElicited[[e]], 
                   data.frame(logtau_0 = log(tau_0), 
                              loglambda_0 = log(lambda_0),
                              delta0 = list_delta_mat[[e]][, 1],
                              delta1 = list_delta_mat[[e]][, 2],
                              delta2 = list_delta_mat[[e]][, 3]
                              )
                   )
  
  list_my_prior[[e]] <- mv_prior
  
  sum_stat_prior[[e]] <- list(mu = round(apply(mv_prior, 2, mean), 3), # location parameters
                              sigma = round(sqrt(diag(cov(mv_prior))), 3), # scale parameters
                              L = round(t(chol(cor(mv_prior))), 3) # cholesky factors
                              )
}

save(list = c("list_my_prior", "sum_stat_prior"), file = "res/expert_prior.RData")

```

These values (locations, scales and lower triangular Cholesky decomposition of the correlation matrix) can be used to specify an informative prior for the heaping process:

```{r}
make_prior <- function(sum_stat, n_sim = 1e4) {
  matrix(rep(sum_stat$mu, n_sim), byrow = TRUE, nrow = n_sim) + 
  matrix(rnorm(5 * n_sim), nrow = n_sim, ncol = 5) %*%
  t(diag(sum_stat$sigma) %*% sum_stat$L)
}
e <- 1
prior <- make_prior(sum_stat_prior[[e]])

```

We can check that this correspond to the answers provided by the expert:

```{r}
### convert back parameters
prior[, 1] <- exp(prior[, 1])
prior[, 2] <- exp(prior[, 2])
prior[, 3:5] <- t(apply(prior[, 3:5], 1, fromDeltaToGamma))
t(apply(prior, 2, summary))
t(apply(do.call('cbind', list_echanParamElicited[[e]]), 2, summary))

dd <- probCondRcppVect(X = 1:400,
                       param_heaping = list(tau_0 = prior[, 1],
                                            lambda_0 = prior[, 2],
                                            gamma = prior[, 3:5]
                                            )
                       )
for(j in 1:length(param_name)) {
  df_j <- data.frame(true_count = 1:400,
                     y = apply(dd[, , j] * (1 - ifelse(j != 1, 1, 0) * dd[, , 1]), 2, mean),
                     lower = as.numeric(apply(dd[, , j] * (1 - ifelse(j != 1, 1, 0) * dd[, , 1]), 2, function(x) { quantile(x, probs = 0.1) })),
                     upper = as.numeric(apply(dd[, , j] * (1 - ifelse(j != 1, 1, 0) * dd[, , 1]), 2, function(x) { quantile(x, probs = 0.9) })),
                     behaviour = rep(param_name[j], 400)
                     )
  df_j$what <- "prior_stan"
  df <- rbind(df, df_j)
}; rm(j, df_j, dd, prior, n_sim)

theme_set(theme_bw(base_size = 16))
df %>% 
  ggplot(aes(x = true_count, y = y, ymin = lower, ymax = upper, fill = behaviour, color = behaviour, linetype = what)) + 
  geom_ribbon(alpha = 0.3) +
  geom_line() +
  scale_x_sqrt(name = "True count", breaks = c(1, 20, 50, 100, 200, 300, 400)) +
  scale_y_continuous(name = "Probability", breaks = seq(0, 1, 0.1)) +
  facet_grid(behaviour ~ what) +
  scale_fill_brewer(name = "Heaping Behaviour: ", type = "qual", palette = "Set2") +
  scale_color_brewer(name = "Heaping Behaviour: ", type = "qual", palette = "Set2") +
  guides(color = "none", linetype = "none") +
  theme(legend.position = "top", 
        plot.title = element_text(lineheight = 0.8, face = "bold"),  
        axis.text.y = element_text(size = 12),
        axis.text.x = element_text(size = 12),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
        )

```

