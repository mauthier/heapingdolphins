
ui <- fluidPage(
  
  # themeSelector(),
  
  # change the of shiny
  theme = shinytheme("lumen"),
  

  mainPanel(
    width = 12,
    
    
    tabsetPanel(
      
      # Question panel ----
      tabPanel("choices_tab",
               sidebarLayout(
                 sidebarPanel(
                   
                   # select language
                   selectInput("language", "language", 
                               c("EN", "FR"),
                               selected = "FR"
                   ),
                   
                   # Interval
                   htmlOutput("intervalUI"),
                   
                   # set as New answers
                   htmlOutput("setNew"),
                   br(),
                   
                   # set as Old answers
                   htmlOutput("setOld"),
                   br(),
                   
                   # export elicitedTable 
                   downloadButton('download',label = uiOutput("dl_elicited")),
                   br(),br(),
                   
                   # determine xmax
                   numericInput("xmax", 
                                label = uiOutput("xmax"),
                                value = 300),
                   
                   # keep question 6 ?
                   htmlOutput("keepLast"),

                 ),
                 
                 mainPanel(
                   div(
                     
                     # Q1
                     h2("Question 1"),
                     conditionalPanel(condition = 'input.intervals == "FALSE"',
                                      sliderInput("Q1_question", 
                                                  label = uiOutput("Q1_a2"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = 10,
                                                  width = '100%') 
                     ),
                     conditionalPanel(condition = 'input.intervals == "TRUE"',
                                      sliderInput("Q1_conf_N", 
                                                  label = uiOutput("Q1_a1"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = c(5,10),
                                                  width = '100%')
                     ),
                     sliderInput("Q1_IC", 
                                 label = uiOutput("Q1_b"), 
                                 min = 1, 
                                 max = 99, 
                                 value = 90,
                                 width = '100%'),
                     br(),
                     
                     # Q2
                     h2("Question 2"),
                     conditionalPanel(condition = 'input.intervals == "FALSE"',
                                      sliderInput("Q2_question", 
                                                  label = uiOutput("Q2_a2"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = 30,
                                                  width = '100%')
                     ),
                     conditionalPanel(condition = 'input.intervals == "TRUE"',
                                      sliderInput("Q2_conf_N", 
                                                  label = uiOutput("Q2_a1"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = c(35,50),
                                                  width = '100%')
                     ),
                     sliderInput("Q2_IC",
                                 label = uiOutput("Q2_b"),
                                 min = 1, 
                                 max = 99, 
                                 value = 90,
                                 width = '100%'),
                     br(),
                     
                     
                     # Q3
                     h2("Question 3"),
                     conditionalPanel(condition = 'input.intervals == "FALSE"',
                                      sliderInput("Q3_question", 
                                                  label = uiOutput("Q3_a2"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = 50,
                                                  width = '100%')
                     ),
                     conditionalPanel(condition = 'input.intervals == "TRUE"',
                                      sliderInput("Q3_conf_N", 
                                                  label = uiOutput("Q3_a1"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = c(50,60),
                                                  width = '100%')             
                     ),
                     sliderInput("Q3_IC",
                                 label = uiOutput("Q3_b"),
                                 min = 1, 
                                 max = 99, 
                                 value = 90,
                                 width = '100%'),
                     br(),
                     
                     
                     # Q4
                     h2("Question 4"),
                     conditionalPanel(condition = 'input.intervals == "FALSE"',
                                      sliderInput("Q4_question", 
                                                  label = uiOutput("Q4_a2"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = 70,
                                                  width = '100%')
                     ),
                     conditionalPanel(condition = 'input.intervals == "TRUE"',
                                      sliderInput("Q4_conf_N", 
                                                  label = uiOutput("Q4_a1"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = c(80,100),
                                                  width = '100%'),          
                     ),
                     sliderInput("Q4_IC",
                                 label = uiOutput("Q4_b"),
                                 min = 1, 
                                 max = 99, 
                                 value = 90,
                                 width = '100%'),
                     br(),
                     
                     
                     # Q5
                     h2("Question 5"),
                     conditionalPanel(condition = 'input.intervals == "FALSE"',
                                      sliderInput("Q5_question", 
                                                  label = uiOutput("Q5_a2"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = 100,
                                                  width = '100%')
                     ),
                     conditionalPanel(condition = 'input.intervals == "TRUE"',
                                      sliderInput("Q5_conf_N", 
                                                  label = uiOutput("Q5_a1"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = c(100,150),
                                                  width = '100%'),            
                     ),
                     sliderInput("Q5_IC",
                                 label = uiOutput("Q5_b"),
                                 min = 1, 
                                 max = 99, 
                                 value = 90,
                                 width = '100%'),
                     br(),
                     
                     
                     # Q6
                     conditionalPanel(condition = 'input.intervals == "FALSE" && input.keepLastQuestion == "TRUE"',
                                      h2("Question 6"),
                                      sliderInput("Q6_question", 
                                                  label = uiOutput("Q6_a2"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = 150,
                                                  width = '100%')
                     ),
                     conditionalPanel(condition = 'input.intervals == "TRUE" && input.keepLastQuestion == "TRUE"',
                                      h2("Question 6"),
                                      sliderInput("Q6_conf_N", 
                                                  label = uiOutput("Q6_a1"), 
                                                  min = 0, 
                                                  max = 700, 
                                                  value = c(150,250),
                                                  width = '100%')            
                     ),
                     conditionalPanel(condition = 'input.keepLastQuestion == "TRUE"',
                                      sliderInput("Q6_IC",
                                                  label = uiOutput("Q6_b"),
                                                  min = 1, 
                                                  max = 99, 
                                                  value = 90,
                                                  width = '100%'),
                     ),
                     br(),
                    
                     style = "font-size:85%"
                   )
                   
                   
                 )
               )
               
               
      ),
      
      # All graphs panel ----
      tabPanel("plot_tab",
               sidebarLayout(
                 sidebarPanel(width = 3,
                              
                   # Clear old to put only New on plot
                   checkboxInput("clearOld_plot_tab", uiOutput("display_old"), value = F),
                   br(),
                   
                   # graph title
                   h4(uiOutput("download_graph")),
                   htmlOutput("text_title_input"),
                   # textInput(inputId = "all_graph_title",
                   #           label = "graph title :",
                   #           value = "heaping_all_graph"),
                   # download graph
                   actionButton("export_all_graph", uiOutput("Export_graph"))
                 ),
                 mainPanel(
                   plotOutput("heaping_graph", width = "100%"),
                   tags$style(type = "text/css", "#heaping_graph {height: calc(100vh - 80px) !important;}")
                 )
               )
               
               
      ),
      
      # Graph selection panel ----
      tabPanel("one graph",
               sidebarLayout(
                 sidebarPanel(width = 3,
                              
                              # graph choice
                              htmlOutput("graphChoice"),
                              
                              # Clear old to put only New on plot
                              checkboxInput("clearOld_one_graph", uiOutput("display_one_Old"), value = F),
                              br(),
                              
                              # graph title
                              h4(uiOutput("download_one_graph")),
                              htmlOutput("text_title_onegraph_input"),
                              # textInput(inputId = "one_graph_title",
                              #           label = "graph title :",
                              #           value = "heaping_one_graph"),
                              
                              # download graph
                              actionButton("export_one_graph", uiOutput("Export_one_graph"))
                 ),
                 mainPanel(
                   plotOutput("heaping_graph_one"),
                   tags$style(type = "text/css", "#heaping_graph_one {height: calc(100vh - 80px) !important;}")
                 )
               )

      )
      
    )
  )
)


