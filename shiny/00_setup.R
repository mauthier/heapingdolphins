# clean environment
rm(list = ls())

#CRAN packages vector
cran_packages <- c(
  # "mapview",
  # "htmlwidgets",
  "devtools",
  "tidyverse",
  "magrittr",
  #"readxl",
  #'rgdal',
  'shiny',
  #'leaflet',
  'RColorBrewer',
  'shinyWidgets',
  'lubridate',
  'DT',
  "pals",
  "shinythemes",
  "glue",
  #"geojsonio",
  #"rmapshaper"
  "openxlsx"
)

github_packages <- c(
  #"mathUtil"
  #"gargle"
)

github_repository <- c(
  # "MathieuGenu/mathUtils"
  # "r-lib/gargle"
)

# c_p_n_i : cran packages not installed
c_p_n_i <- cran_packages[!(cran_packages %in% installed.packages())]
g_p_n_i <- which(!(github_packages %in% installed.packages()))


# installation of packages
lapply(c_p_n_i, install.packages, dependencies = TRUE)
lapply(github_repository[g_p_n_i], devtools::install_github , dependencies = TRUE)

#install packages
lapply(c(cran_packages,github_packages), function(x){                                 
  library(x, character.only = TRUE, quietly = TRUE)
})

rm(c_p_n_i, g_p_n_i, cran_packages, github_packages, github_repository)
