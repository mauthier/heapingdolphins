##--------------------------------------------------------------------------------------------------------
## SCRIPT : Run simulations with Stan
##
## Authors : Isabelle Albert, Matthieu Authier, Daouda Ba, Sophie Donnet, Mathieu Genu & Eric Parent
##
## Last update : 2022-01-18
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("ggplot2", "ggthemes", "tidyverse"), library, character.only = TRUE)

rm(list = ls())

# source('functions/functionsHeapingModel.R')
# source('functions/functionsHeapingProbabilityRcpp.R')
# 
# ### load functions for Stan
# source('functions/functionsStan.R')
# source('functions/functionsStanmodels.R')

#--------------- SIMULATING DATA with covariates and 3 observers ----#
# n_observer <- 3
n_obs <- c(100, 200, 400)
# n_tot <- n_observer * n_obs
# 
# mu <- 20 # mean
# omega <- 15 # overdispersion
# heaping_levels <- c(1, 5, 10, 50)
# 
# n_cov <- 2
# beta_cov <- c(-log(2), log(5) / 2)
# 
# make_Xcov <- function(n_obs, n_observer, n_cov) {
#   Xcov <- vector(mode = 'list', length = n_observer)
#   for(k in 1:n_observer) {
#     Xcov[[k]] <- cbind(rep(1, n_obs), replicate(n_cov, rnorm(n_obs) + rbinom(n_obs, size = 1, prob = 0.05)))
#   }
#   return(Xcov)
# }
# 
# #--------------------------------------------------------------------#
# # first observer
# param_obs1 = list()
# param_obs1$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
#                                  beta = c(log(mu - 1), beta_cov) # because shifted distribution
#                                  )
# 
# param_obs1$param_heaping <- list(lambda_0 = 0.1,
#                                  tau_0 = 7,
#                                  threshold = 0.05,
#                                  gamma = c(0.2, -11.5, -35.0)
#                                  )
# 
# # second observer
# param_obs2 = list()
# param_obs2$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
#                                  beta = c(log(mu - 1), beta_cov) # because shifted distribution
#                                  )
# 
# param_obs2$param_heaping <- list(lambda_0 = 0.3,
#                                  tau_0 = 22,
#                                  threshold = 0.05,
#                                  gamma = c(0.3, -5.0, -65.0)
#                                  )
# 
# # third observer
# param_obs3 = list()
# param_obs3$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
#                                  beta = c(log(mu - 1), beta_cov) # because shifted distribution
#                                  )
# 
# param_obs3$param_heaping <- list(lambda_0 = 0.3,
#                                  tau_0 = 12,
#                                  threshold = 0.05,
#                                  gamma = c(0.2, -6.0, -20.0)
#                                  )
# param_sim <- list(param_obs1, param_obs2, param_obs3)
distrib <- c("Poisson", "Negbin") # data-generating mechanism
model <- "mheap"#c("null", "heap", "mheap")

### check convergence
check_conv <- function(fit, param) {
  n_chains <- fit@sim$chains
  n_iter <- (fit@sim$iter -fit@sim$warmup) / fit@sim$thin
  x <- rstan::extract(fit, param)[[1]]
  if(length(dim(x)) == 1) {
    y <- array(NA, dim = c(n_iter, n_chains))
    for(j in 1:n_chains) {
      y[, j] <- x[(j - 1) * n_iter + 1:n_iter]
    }
    out <- rstan::Rhat(sims = y)
  } 
  if(length(dim(x)) == 2) {
    y <- array(NA, dim = c(n_iter, n_chains, dim(x)[-1]))
    for(j in 1:n_chains) {
      y[, j, ] <- x[(j - 1) * n_iter + 1:n_iter, ]
    }
    out <- apply(y, 3, rstan::Rhat)
  }
  if(length(dim(x)) == 3) {
    y <- array(NA, dim = c(n_iter, n_chains, dim(x)[-1]))
    for(j in 1:n_chains) {
      y[, j, , ] <- x[(j - 1) * n_iter + 1:n_iter, , ]
    }
    dd <- apply(y, c(3, 4), rstan::Rhat)
    out <- NULL
    for(k in 1:nrow(dd)) {
      out <- c(out, dd[k, ])
    }
  }
  return(out)
}

get_pointestimate <- function(fit) {
  par_name <- c("intercept", "slope")
  output <- data.frame(mu = mean(exp(rstan::extract(fit, 'intercept')$intercept)),
                       beta_1 = apply(rstan::extract(fit, 'slope')$slope, 2, mean)[1],
                       beta_2 = apply(rstan::extract(fit, 'slope')$slope, 2, mean)[2]
                       )
  if(any(fit@model_pars == "tau_0")) {
    par_name <- c(par_name, "tau_0", "lambda_0", "gamma")
    if(length(fit@par_dims$gamma) != 1) {
      output <- cbind(output,
                      data.frame(obs1_tau_0 = apply(rstan::extract(fit, 'tau_0')$tau_0, 2, mean)[1],
                                 obs2_tau_0 = apply(rstan::extract(fit, 'tau_0')$tau_0, 2, mean)[2],
                                 obs3_tau_0 = apply(rstan::extract(fit, 'tau_0')$tau_0, 2, mean)[3],
                                 obs1_lambda_0 = apply(rstan::extract(fit, 'lambda_0')$lambda_0, 2, mean)[1],
                                 obs2_lambda_0 = apply(rstan::extract(fit, 'lambda_0')$lambda_0, 2, mean)[2],
                                 obs3_lambda_0 = apply(rstan::extract(fit, 'lambda_0')$lambda_0, 2, mean)[3],
                                 obs1_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[1, 1],
                                 obs1_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[1, 2],
                                 obs1_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[1, 3],
                                 obs2_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[2, 1],
                                 obs2_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[2, 2],
                                 obs2_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[2, 3],
                                 obs3_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[3, 1],
                                 obs3_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[3, 2],
                                 obs3_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[3, 3]
                                 )
                      )
    } else {
      output <- cbind(output,
                      data.frame(obs1_tau_0 = mean(rstan::extract(fit, 'tau_0')$tau_0),
                                 obs1_lambda_0 = mean(rstan::extract(fit, 'lambda_0')$lambda_0),
                                 obs1_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, 2, mean)[1],
                                 obs1_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, 2, mean)[2],
                                 obs1_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, 2, mean)[3]
                                 )
                      )
      }
    }
  if(any(fit@model_pars == "overdispersion")) {
    par_name <- c(par_name, "overdispersion")
    output$overdispersion <- mean(rstan::extract(fit, 'overdispersion')$overdispersion)
    } 
  ### check convergence
  rhat <- do.call('c', lapply(par_name, check_conv, fit = fit))
  output$looic <- loo::loo(loo::extract_log_lik(fit))$estimates[3, 1]
  if(any(rhat > 1.05)) {
    output[1, ] <- rep(NA, ncol(output))
  }
  return(output)
}

get_results <- function(l, j, m) {
  load(paste("D:/Heaping/stan_data_", l, "_n", j, "_", m, ".RData", sep = ""))
  temp <- do.call('rbind', 
                  lapply(get(paste("poisson", m, sep = "_")), 
                         get_pointestimate
                         )
                  )
  par_name <- names(temp)
  temp %>% 
    mutate(sim = 1:n(),
           sample_size = j
           ) %>%
    pivot_longer(cols = all_of(par_name),
                 names_to = "param",
                 values_to = "estimate"
                 ) %>% 
    mutate(model = paste("poisson", m, sep = "_"),
           truth = l
           ) %>% 
    as.data.frame() %>%
    write.table(file = paste("res/results_", l, "_", j, "_", m, "_poisson.txt", sep = ""),
                quote = FALSE, col.names = TRUE, row.names = FALSE, sep = "\t"
                )
  ### negbin
  temp <- do.call('rbind', 
                  lapply(get(paste("negbin", m, sep = "_")), 
                         get_pointestimate
                         )
                  )
  par_name <- names(temp)
  temp %>% 
    mutate(sim = 1:n(),
           sample_size = j
           ) %>%
    pivot_longer(cols = all_of(par_name),
                 names_to = "param",
                 values_to = "estimate"
                 ) %>% 
    mutate(model = paste("negbin", m, sep = "_"),
           truth = l
           ) %>% 
    as.data.frame() %>%
    write.table(file = paste("res/results_", l, "_", j, "_", m, "_negbin.txt", sep = ""),
                quote = FALSE, col.names = TRUE, row.names = FALSE, sep = "\t"
                )
}

for(l in distrib) {
  message(paste("Likelihood:", l, sep = " "))
  for(j in n_obs) {
    message(paste("\t sample size:", j, sep = " "))
    for(m in model) {
      message(paste("\t\t model:", m, sep = " "))
      get_results(l = l, j = j, m = m)
    }
  }
}

# for(l in distrib) {
#   message(paste("Likelihood:", l, sep = " "))
#   for(j in n_obs) {
#     message(paste("\t sample size:", j, sep = " "))
#     # data
#     # load(paste("D:/Heaping/simul_data_", l, "_n", j, ".RData", sep = ""))
#     for(m in model) {
#       load(paste("D:/Heaping/stan_data_", l, "_n", j, "_", m, ".RData", sep = ""))
#       temp <- do.call('rbind', 
#                       lapply(get(paste("poisson", m, sep = "_")), 
#                              get_pointestimate)
#                       )
#       par_name <- names(temp)
#       all_results <- rbind(all_results,
#                            temp %>% 
#                              mutate(sim = 1:n(),
#                                     sample_size = j
#                                     ) %>%
#                              pivot_longer(cols = all_of(par_name),
#                                           names_to = "param",
#                                           values_to = "estimate"
#                                           ) %>% 
#                              mutate(model = paste("poisson", m, sep = "_"),
#                                     truth = l
#                                     ) %>% 
#                              as.data.frame()
#                            )
#       ### negbin
#       temp <- do.call('rbind', 
#                       lapply(get(paste("negbin", m, sep = "_")), 
#                              get_pointestimate)
#                       )
#       par_name <- names(temp)
#       all_results <- rbind(all_results,
#                            temp %>% 
#                              mutate(sim = 1:n(),
#                                     sample_size = j
#                                     ) %>%
#                              pivot_longer(cols = all_of(par_name),
#                                           names_to = "param",
#                                           values_to = "estimate"
#                                           ) %>% 
#                              mutate(model = paste("negbin", m, sep = "_"),
#                                     truth = l
#                                     ) %>% 
#                              as.data.frame()
#                            )
#       rm(list = ls()[-match(x = c("check_conv", "get_pointestimate",
#                                 "j", "l", "m",
#                                 "distrib", "n_obs", "model",
#                                 "all_results"
#                                 ), 
#                           table = ls())]
#          )
#       gc()
#     }
#   }
# }; rm(m, j, l)
# 
# save(file = paste("res/all_simul_results.RData", sep = ""),
#      list = c("all_results")
#      )

### aggregate all results
p_results <- get_results(n = n_obs, likelihood = "Poisson")
nb_results <- get_results(n = n_obs, likelihood = "Negbin")

ls()
str(p_results)
table(p_results$model)
table(nb_results$param)

### compute probability to select the true DGM
p_results %>% 
  filter(param == "looic") %>% 
  pivot_wider(names_from = model, values_from = estimate) %>% 
  group_by(sample_size, sim) %>% 
  mutate(delta = ifelse(negbin < poisson, 1, 0)) %>% 
  group_by(sample_size) %>% 
  summarize(proba = mean(delta, na.rm = TRUE),
            conv = sum(!is.na(delta))
            )
# # A tibble: 3 x 3
#   sample_size proba  conv
# 1         200     1   100
# 2         500     1    95
# 3        1000     1    85

nb_results %>% 
  filter(param == "looic") %>% 
  pivot_wider(names_from = model, values_from = estimate) %>% 
  group_by(sample_size, sim) %>% 
  mutate(delta = ifelse(negbin < poisson, 1, 0)) %>% 
  group_by(sample_size) %>% 
  summarize(proba = mean(delta, na.rm = TRUE),
            conv = sum(!is.na(delta))
            )
# A tibble: 3 x 3
#   sample_size proba  conv
# 1         200     1    93
# 2         500     1    55
# 3        1000     1    30

### check convergence
rbind(p_results, 
      nb_results
      ) %>% 
  filter(param == "looic") %>% 
  group_by(likelihood, model, sample_size) %>% 
  summarize(conv = sum(!is.na(estimate)))
# A tibble: 12 x 4
# Groups:   likelihood, model [4]
#    likelihood model   sample_size  conv
#  1 Negbin     negbin          200   100
#  2 Negbin     negbin          500   100
#  3 Negbin     negbin         1000   100
#  4 Negbin     poisson         200    93
#  5 Negbin     poisson         500    55
#  6 Negbin     poisson        1000    30
#  7 Poisson    negbin          200   100
#  8 Poisson    negbin          500   100
#  9 Poisson    negbin         1000   100
# 10 Poisson    poisson         200   100
# 11 Poisson    poisson         500    95
# 12 Poisson    poisson        1000    85
### save this
save.image(paste("res/20210407_SimulationsStan_wo_cov.RData"), safe = TRUE)

p_results %>% 
  filter(param %in% c("tau_0", "lambda_0", "gamma_0", "gamma_1", "gamma_2")) %>% 
  group_by(model, sample_size, param) %>% 
  summarize(x = mean(estimate, na.rm = TRUE))

theme_set(theme_bw(base_size = 12))
p_results %>% 
  filter(param %in% c("gamma_0", "gamma_1", "gamma_2", "tau_0", "lambda_0")) %>% 
  ggplot(aes(x = sample_size, y = estimate, group = sample_size)) +
  geom_violin() +
  geom_hline(data = data.frame(estimate = rep(c(0.2, -10.0, -25.0, 12, 0.10), 2),
                               param = rep(c("gamma_0", "gamma_1", "gamma_2", "tau_0", "lambda_0"), 2),
                               likelihood = rep(c("negbin", "poisson"), each = 5)
                               ),
             aes(yintercept = estimate), color = "tomato"
             ) + 
  geom_hline(data = data.frame(estimate = rep(c(fromDeltaToGamma(c(log(0.2), 0, log(10))), 10, 0.5), 2),
                               param = rep(c("gamma_0", "gamma_1", "gamma_2", "tau_0", "lambda_0"), 2),
                               likelihood = rep(c("negbin", "poisson"), each = 5)
                               ),
             aes(yintercept = estimate), color = "midnightblue", linetype = "dashed"
             ) +
  scale_x_log10(name = "sample size", breaks = c(200, 500, 1000)) +
  facet_grid(param ~ model, scales = "free") +
  labs(title = "A",
       subtitle = "Poisson\ndata-generating-process"
       ) +
  theme(legend.position = "top", 
        plot.title = element_text(lineheight = 0.8, face = "bold"),  
        axis.text.y = element_text(size = 10),
        axis.text.x = element_text(size = 10, angle = 45),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
        ) 

nb_results %>% 
  filter(param %in% c("tau_0", "lambda_0", "gamma_0", "gamma_1", "gamma_2")) %>% 
  group_by(model, sample_size, param) %>% 
  summarize(x = mean(estimate, na.rm = TRUE))

theme_set(theme_bw(base_size = 12))
nb_results %>% 
  filter(param %in% c("gamma_0", "gamma_1", "gamma_2", "tau_0", "lambda_0")) %>% 
  ggplot(aes(x = sample_size, y = estimate, group = sample_size)) +
  geom_violin() +
  geom_hline(data = data.frame(estimate = rep(c(0.2, -10.0, -25.0, 12, 0.10), 2),
                               param = rep(c("gamma_0", "gamma_1", "gamma_2", "tau_0", "lambda_0"), 2),
                               likelihood = rep(c("negbin", "poisson"), each = 5)
                               ),
             aes(yintercept = estimate), color = "tomato"
             ) + 
  geom_hline(data = data.frame(estimate = rep(c(fromDeltaToGamma(c(log(0.2), 0, log(10))), 10, 0.5), 2),
                               param = rep(c("gamma_0", "gamma_1", "gamma_2", "tau_0", "lambda_0"), 2),
                               likelihood = rep(c("negbin", "poisson"), each = 5)
                               ),
             aes(yintercept = estimate), color = "midnightblue", linetype = "dashed"
             ) +
  scale_x_log10(name = "sample size", breaks = c(200, 500, 1000)) +
  facet_grid(param ~ model, scales = "free") +
  labs(title = "B", 
       subtitle = "Negative Binomial\ndata-generating-process"
       ) +
  theme(legend.position = "top", 
        plot.title = element_text(lineheight = 0.8, face = "bold"),  
        axis.text.y = element_text(size = 10),
        axis.text.x = element_text(size = 10, angle = 45),
        panel.grid.minor = element_blank(),
        panel.background = element_blank()
        ) 
