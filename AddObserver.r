##--------------------------------------------------------------------------------------------------------
## SCRIPT : Add observer ID to Data
##
## Authors : Matthieu Authier
##
## Last update : 2021-10-11
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("ggplot2", "ggthemes", "tidyverse", "readxl"), library, character.only = TRUE)

rm(list = ls())

obs1 <- read_excel(path = "data/20200518_HeapingExtraction.xls")
obs2 <- read_excel(path = "data/20200519_HeapingExtraction.xls")

### keep only the observer
obs1$OBS <- NA
for(i in 1:nrow(obs1)) {
  if(obs1$`N observateurs`[i] == 2) {
    obs1$OBS[i] <- ifelse(obs1$cote[i] == "T", obs1$obs_tribord[i], obs1$obs_babord[i])
  } else {
    obs1$OBS[i] <- ifelse(is.na(obs1$obs_babord)[i], obs1$obs_tribord[i], obs1$OBS[i])
    obs1$OBS[i] <- ifelse(is.na(obs1$obs_tribord)[i], obs1$obs_babord[i], obs1$OBS[i])
  }
}
table(obs1$OBS)
obs1 %>%
  skimr::skim()
obs1 %>%
  filter(is.na(OBS)) %>%
  View()

obs2$OBS <- NA
for(i in 1:nrow(obs1)) {
  if(obs2$`N observateurs`[i] == 2) {
    obs2$OBS[i] <- ifelse(obs2$`coté`[i] == "T", obs2$obs_tribord[i], obs2$obs_babord[i])
  } else {
    obs2$OBS[i] <- ifelse(is.na(obs2$obs_babord)[i], obs2$obs_tribord[i], obs2$OBS[i])
    obs2$OBS[i] <- ifelse(is.na(obs2$obs_tribord)[i], obs2$obs_babord[i], obs2$OBS[i])
  }
}
table(obs2$OBS)
obs2 %>%
  skimr::skim()
obs2 %>%
  filter(is.na(OBS)) %>%
  View()

allobs <- rbind(obs1 %>% 
                  select(Survey, an, mois, jour, code_esp, nombre, vitesse, plateforme, beaufort, Longitude, Latitude, cptmt_MM, OBS) %>%
                  mutate(Longitude = as.character(Longitude),
                         Latitude = as.character(Latitude)
                         ),
                obs2 %>% 
                  select(Survey, an, mois, jour, code_esp, nombre, vitesse, plateforme, beaufort, Longitude, Latitude, cptmt_MM, OBS) %>%
                  mutate(Longitude = as.character(Longitude),
                         Latitude = as.character(Latitude)
                         )
                ) %>%
  mutate(plateforme = ifelse(plateforme %in% c("pont_supérieur", "PoNt_supérieur"), "pont_sup", plateforme),
         plateforme = ifelse(plateforme %in% c("passerelle", "Passerelle"), "passerelle", plateforme)
         ) %>%
  left_join(read_excel(path = "data/20200520_Megascope.xls") %>%
              mutate(plateforme = ifelse(plateforme %in% c("pont_supTrieur", "pont_supIrieur", "pont_supirieur", "PoNt_supIrieur"), "pont_sup", plateforme),
                     plateforme = ifelse(plateforme %in% c("passerelle", "Passerelle"), "passerelle", plateforme)
                     ) %>%
              distinct(),
            by = c("Survey", "an", "mois", "jour", "code_esp", "nombre", "vitesse", "plateforme", "beaufort", "Longitude", "Latitude", "cptmt_MM")
            ) %>%
  filter(!is.na(Dist2Cote),
         !is.na(Dist2B200),
         !is.na(Slope),
         !is.na(Depth)
         ) %>%
  select(-activite, -min, -max, -N_observat) %>%
  arrange(an, mois, jour, Survey) %>%
  mutate(OBS = ifelse(OBS == "PM", "PMF", OBS))

table(allobs$OBS)

hist(allobs$nombre)
