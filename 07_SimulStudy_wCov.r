##--------------------------------------------------------------------------------------------------------
## SCRIPT : Run simulations with Stan
##
## Authors : Isabelle Albert, Matthieu Authier, Daouda Ba, Sophie Donnet, Mathieu Genu & Eric Parent
##
## Last update : 2021-10-02
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("ggplot2", "ggthemes", "tidyverse"), library, character.only = TRUE)

rm(list = ls())

source('functions/functionsHeapingModel.R')
source('functions/functionsHeapingProbabilityRcpp.R')

### load functions for Stan
source('functions/functionsStan.R')
source('functions/functionsStanmodels.R')

#--------------- SIMULATING DATA with covariates and 3 observers ----#
n_observer <- 3
n_obs <- c(100, 200, 400)
n_tot <- n_observer * n_obs

mu <- 20 # mean
omega <- 15 # overdispersion
heaping_levels <- c(1, 5, 10, 50)

n_cov <- 2
beta_cov <- c(-log(2), log(5) / 2)

make_Xcov <- function(n_obs, n_observer, n_cov) {
  Xcov <- vector(mode = 'list', length = n_observer)
  for(k in 1:n_observer) {
    Xcov[[k]] <- cbind(rep(1, n_obs), replicate(n_cov, rnorm(n_obs) + rbinom(n_obs, size = 1, prob = 0.05)))
  }
  return(Xcov)
}

#--------------------------------------------------------------------#
# first observer
param_obs1 = list()
param_obs1$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
                                 beta = c(log(mu - 1), beta_cov) # because shifted distribution
                                 )

param_obs1$param_heaping <- list(lambda_0 = 0.1,
                                 tau_0 = 7,
                                 threshold = 0.05,
                                 gamma = c(0.2, -11.5, -35.0)
                                 )

# second observer
param_obs2 = list()
param_obs2$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
                                 beta = c(log(mu - 1), beta_cov) # because shifted distribution
                                 )

param_obs2$param_heaping <- list(lambda_0 = 0.3,
                                 tau_0 = 22,
                                 threshold = 0.05,
                                 gamma = c(0.3, -5.0, -65.0)
                                 )

# third observer
param_obs3 = list()
param_obs3$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
                                 beta = c(log(mu - 1), beta_cov) # because shifted distribution
                                 )

param_obs3$param_heaping <- list(lambda_0 = 0.3,
                                 tau_0 = 12,
                                 threshold = 0.05,
                                 gamma = c(0.2, -6.0, -20.0)
                                 )
param_sim <- list(param_obs1, param_obs2, param_obs3)
distrib <- c("Poisson", "Negbin") # data-generating mechanism

### for reproducibility
set.seed(20211002)
n_sim <- 1e2
seeds <- sample.int(1e6, size = n_sim, replace = FALSE)

### generate the data
for(l in rev(distrib)) {
  message(paste("Data-Generating Mechanism:", l, sep = " "))
  for(j in rev(n_obs)) {
    message(paste("\t sample size:", j, sep = " "))
    ### generate data
    all_covar <- all_data <- vector(mode = 'list', length = n_sim)
    for(i in 1:n_sim) {
      set.seed(seeds[i])
      all_covar[[i]] <- make_Xcov(n_obs = j, n_observer = n_observer, n_cov = n_cov)
      temp <- vector(mode = 'list', length = n_observer)
      for(k in 1:n_observer) {
        dd <- simulHeapedDataCovar(covar = all_covar[[i]][[k]],
                                   param = param_sim[[k]],
                                   heaping_levels = heaping_levels,
                                   model = list(distrib = ifelse(l == "Poisson", 'pois', 'nbinom'),
                                                heaping = 'twoStepsRcpp'
                                                )
                                   )
        while(any(is.na(dd$Y))) {
          all_covar[[i]] <- make_Xcov(n_obs = j, n_observer = n_observer, n_cov = n_cov)
          dd <- simulHeapedDataCovar(covar = all_covar[[i]][[k]],
                                     param = param_sim[[k]],
                                     heaping_levels = heaping_levels,
                                     model = list(distrib = ifelse(l == "Poisson", 'pois', 'nbinom'),
                                                  heaping = 'twoStepsRcpp'
                                                  )
                                     )
        }
        dd$Observer <- k
        covX <- as.data.frame(all_covar[[i]][[k]][, -1])
        names(covX) <- paste("cov", 1:ncol(covX), sep = "")
        temp[[k]] <- cbind(dd, covX); rm(dd, covX)
      }
      all_data[[i]] <- do.call('rbind', temp); rm(temp)
    }
    save(list = c("all_data"), file = paste("res/simul_data_", l, "_n", j, ".RData", sep = ""))
  }
}

### check the simulations
### heaping percentage
percent <- do.call('rbind',
                   lapply(all_data, function(x) { sapply(1:4, function(G) { sum(x$sim_G == G) } ) / nrow(x) })
                   )
percent <- as.data.frame(percent); names(percent) <- c("no_heaping", "heap_at_5", "heap_at_10", "heap_at_50")
skimr::skim(percent)
### summary statistics
sum_stat <- do.call('rbind', lapply(1:n_sim, function(x) {
  c(mean(all_data[[x]]$X), sd(all_data[[x]]$X), max(all_data[[x]]$X), 
    mean(all_data[[x]]$Y), sd(all_data[[x]]$Y), max(all_data[[x]]$Y)
    ) 
  }
)
)
sum_stat <- as.data.frame(sum_stat); names(sum_stat) <- c("mean_X", "sd_X", "max_X", "mean_Y", "sd_Y", "max_Y")
skimr::skim(sum_stat)

### bayesian estimation (6 models to fit)
get_bayes <- function(all_data,
                      n,
                      distrib,
                      n_chain = 4,
                      n_iter = 1e3,
                      n_warm = 500,
                      n_thin = 1,
                      hmc_control = list(adapt_delta = 0.9)
                      ) {
  poisson_null <- negbin_null <- vector(mode = 'list', length = length(all_data))
  poisson_heap <- negbin_heap <- vector(mode = 'list', length = length(all_data))
  poisson_mheap <- negbin_mheap <- vector(mode = 'list', length = length(all_data))
  ### fit the models
  for(i in 1:length(all_data)) {
    ### null model
    poisson_null[[i]] <- sampling(stan_nullpoisson_cov,
                                  data = list(n_obs = length(all_data[[i]]$Y),
                                              n_y = as.numeric(all_data[[i]]$Y),
                                              Y = as.numeric(all_data[[i]]$Y),
                                              prior_location_intercept = log(10),
                                              prior_scale_intercept = log(5) / 2,
                                              n_cov = 2,
                                              prior_location_slope = rep(0, 2),
                                              prior_scale_slope = rep(log(2) / 2, 2),
                                              Z = as.matrix(all_data[[i]][, grep("cov", names(all_data[[i]]))])
                                              ),
                                  pars = c("intercept", "slope", "log_lik"),
                                  chains = n_chain,
                                  iter = n_iter,
                                  warmup = n_warm,
                                  thin = n_thin,
                                  control = hmc_control
                                  )
    negbin_null[[i]] <- sampling(stan_nullnegbin_cov,
                                 data = list(n_obs = length(all_data[[i]]$Y),
                                             n_y = as.numeric(all_data[[i]]$Y),
                                             Y = as.numeric(all_data[[i]]$Y),
                                             prior_location_intercept = log(10),
                                             prior_scale_intercept = log(5) / 2,
                                             n_cov = 2,
                                             prior_location_slope = rep(0, 2),
                                             prior_scale_slope = rep(log(2), 2),
                                             Z = as.matrix(all_data[[i]][, grep("cov", names(all_data[[i]]))])
                                             ),
                                 pars = c("intercept", "slope", "overdispersion", "log_lik"),
                                 chains = n_chain,
                                 iter = n_iter,
                                 warmup = n_warm,
                                 thin = n_thin,
                                 control = hmc_control
                                 )
    ### one observer
    standatalist <- data4stan(countdata = all_data[[i]]$Y,
                              covar = cbind(rep(1, nrow(all_data[[i]])), as.matrix(all_data[[i]][, grep("cov", names(all_data[[i]]))])),
                              heaping_levels = heaping_levels,
                              prior_location_intercept = log(10),
                              prior_scale_intercept = log(5) / 2,
                              prior_location = c(log(10), log(0.5), log(0.2), 0, log(10)),
                              prior_cholmat = diag(c(log(3) ,log(5) /2, log(5) / 2, 5.0, log(5) / 2)) %*%
                                diag(5),
                              threshold = 0.05
                              )

    poisson_heap[[i]] <- sampling(stan_poisson_cov,
                                  data = standatalist,
                                  pars = c("intercept", "slope", "gamma", "tau_0", "lambda_0", "sup_heaping", "log_lik"),
                                  chains = n_chain,
                                  iter = n_iter,
                                  warmup = n_warm,
                                  thin = n_thin,
                                  control = hmc_control
                                  )

    negbin_heap[[i]] <- sampling(stan_negbin_cov,
                                 data = standatalist,
                                 pars = c("intercept", "slope", "gamma", "tau_0", "lambda_0", "sup_heaping", "overdispersion", "log_lik"),
                                 chains = n_chain,
                                 iter = n_iter,
                                 warmup = n_warm,
                                 thin = n_thin,
                                 control = hmc_control
                                 )
    ### several observers
    prior_cholmat <- array(NA, dim = c(length(unique(all_data[[i]]$Observer)), 5, 5))
    for(k in 1:length(unique(all_data[[i]]$Observer))) {
      prior_cholmat[k, , ] <- diag(c(log(3) ,log(5) /2, log(5) / 2, 5.0, log(5) / 2)) %*% diag(5)
    }
    standatalist <- data4stan(countdata = all_data[[i]]$Y,
                              covar = cbind(rep(1, nrow(all_data[[i]])), as.matrix(all_data[[i]][, grep("cov", names(all_data[[i]]))])),
                              heaping_levels = heaping_levels,
                              prior_location_intercept = log(10),
                              prior_scale_intercept = log(5) / 2,
                              prior_location = matrix(rep(c(log(10), log(0.5), log(0.2), 0, log(10)), 
                                                          length(unique(all_data[[i]]$Observer))
                                                          ),
                                                      nrow = 3, byrow = TRUE
                                                      ),
                              prior_cholmat = prior_cholmat,
                              threshold = 0.05
                              )
    standatalist$n_observer <- length(unique(all_data[[i]]$Observer))
    standatalist$OBS <- all_data[[i]]$Observer
    
    poisson_mheap[[i]] <- sampling(stan_poisson_cov_multi,
                                   data = standatalist,
                                   pars = c("intercept", "slope", "gamma", "tau_0", "lambda_0", "sup_heaping", "log_lik"),
                                   chains = n_chain,
                                   iter = n_iter,
                                   warmup = n_warm,
                                   thin = n_thin,
                                   control = hmc_control
                                   )
    
    negbin_mheap[[i]] <- sampling(stan_negbin_cov_multi,
                                  data = standatalist,
                                  pars = c("intercept", "slope", "gamma", "tau_0", "lambda_0", "sup_heaping", "overdispersion", "log_lik"),
                                  chains = n_chain,
                                  iter = n_iter,
                                  warmup = n_warm,
                                  thin = n_thin,
                                  control = hmc_control
                                  )
    
  }
  save(file = paste("res/stan_data_", distrib, "_n", n, ".RData", sep = ""),
       list = c("poisson_null", "negbin_null", 
                "poisson_heap", "negbin_heap",
                "poisson_mheap", "negbin_mheap"
                )
       )
}

for(l in distrib) {
  message(paste("Likelihood:", l, sep = " "))
  for(j in n_obs) {
    message(paste("\t sample size:", j, sep = " "))
    load(paste("res/simul_data_", l, "_n", j, ".RData", sep = ""))
    get_bayes(all_data = all_data, n = j, distrib = l)
  }
}; rm(i, j, l)
