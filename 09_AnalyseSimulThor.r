##--------------------------------------------------------------------------------------------------------
## SCRIPT : Run simulations with Stan
##
## Authors : Isabelle Albert, Matthieu Authier, Daouda Ba, Sophie Donnet, Mathieu Genu & Eric Parent
##
## Last update : 2022-01-18
## R version 4.0.5 (2021-03-31) -- "Shake and Throw"
## Copyright (C) 2021 The R Foundation for Statistical Computing
## Platform: x86_64-w64-mingw32/x64 (64-bit)
##--------------------------------------------------------------------------------------------------------

lapply(c("tidyverse", "rstan", "loo"), library, character.only = TRUE)

rm(list = ls())

# source('functions/functionsHeapingModel.R')
# source('functions/functionsHeapingProbabilityRcpp.R')
# 
# ### load functions for Stan
# source('functions/functionsStan.R')
# source('functions/functionsStanmodels.R')

#--------------- SIMULATING DATA with covariates and 3 observers ----#
# n_observer <- 3
n_obs <- c(100, 200, 400)
# n_tot <- n_observer * n_obs
# 
# mu <- 20 # mean
# omega <- 15 # overdispersion
# heaping_levels <- c(1, 5, 10, 50)
# 
# n_cov <- 2
# beta_cov <- c(-log(2), log(5) / 2)
# 
# make_Xcov <- function(n_obs, n_observer, n_cov) {
#   Xcov <- vector(mode = 'list', length = n_observer)
#   for(k in 1:n_observer) {
#     Xcov[[k]] <- cbind(rep(1, n_obs), replicate(n_cov, rnorm(n_obs) + rbinom(n_obs, size = 1, prob = 0.05)))
#   }
#   return(Xcov)
# }
# 
# #--------------------------------------------------------------------#
# # first observer
# param_obs1 = list()
# param_obs1$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
#                                  beta = c(log(mu - 1), beta_cov) # because shifted distribution
#                                  )
# 
# param_obs1$param_heaping <- list(lambda_0 = 0.1,
#                                  tau_0 = 7,
#                                  threshold = 0.05,
#                                  gamma = c(0.2, -11.5, -35.0)
#                                  )
# 
# # second observer
# param_obs2 = list()
# param_obs2$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
#                                  beta = c(log(mu - 1), beta_cov) # because shifted distribution
#                                  )
# 
# param_obs2$param_heaping <- list(lambda_0 = 0.3,
#                                  tau_0 = 22,
#                                  threshold = 0.05,
#                                  gamma = c(0.3, -5.0, -65.0)
#                                  )
# 
# # third observer
# param_obs3 = list()
# param_obs3$param_distrib <- list(transfo_over_dispersion = log(omega - 1),
#                                  beta = c(log(mu - 1), beta_cov) # because shifted distribution
#                                  )
# 
# param_obs3$param_heaping <- list(lambda_0 = 0.3,
#                                  tau_0 = 12,
#                                  threshold = 0.05,
#                                  gamma = c(0.2, -6.0, -20.0)
#                                  )
# param_sim <- list(param_obs1, param_obs2, param_obs3)
distrib <- c("Poisson", "Negbin") # data-generating mechanism
model <- c("null", "heap", "mheap")

### check convergence
check_conv <- function(fit, param) {
  n_chains <- fit@sim$chains
  n_iter <- (fit@sim$iter -fit@sim$warmup) / fit@sim$thin
  x <- rstan::extract(fit, param)[[1]]
  if(length(dim(x)) == 1) {
    y <- array(NA, dim = c(n_iter, n_chains))
    for(j in 1:n_chains) {
      y[, j] <- x[(j - 1) * n_iter + 1:n_iter]
    }
    out <- rstan::Rhat(sims = y)
  } 
  if(length(dim(x)) == 2) {
    y <- array(NA, dim = c(n_iter, n_chains, dim(x)[-1]))
    for(j in 1:n_chains) {
      y[, j, ] <- x[(j - 1) * n_iter + 1:n_iter, ]
    }
    out <- apply(y, 3, rstan::Rhat)
  }
  return(out)
}

get_pointestimate <- function(fit) {
  par_name <- c("intercept", "slope")
  output <- data.frame(mu = mean(exp(rstan::extract(fit, 'intercept')$intercept)),
                       beta_1 = apply(rstan::extract(fit, 'slope')$slope, 2, mean)[1],
                       beta_2 = apply(rstan::extract(fit, 'slope')$slope, 2, mean)[2]
                       )
  if(any(fit@model_pars == "tau_0")) {
    par_name <- c(par_name, "tau_0", "lambda_0", "gamma")
    if(length(fit@par_dims$gamma) != 1) {
      output <- cbind(output,
                      data.frame(obs1_tau_0 = apply(rstan::extract(fit, 'tau_0')$tau_0, 2, mean)[1],
                                 obs2_tau_0 = apply(rstan::extract(fit, 'tau_0')$tau_0, 2, mean)[2],
                                 obs3_tau_0 = apply(rstan::extract(fit, 'tau_0')$tau_0, 2, mean)[3],
                                 obs1_lambda_0 = apply(rstan::extract(fit, 'lambda_0')$lambda_0, 2, mean)[1],
                                 obs2_lambda_0 = apply(rstan::extract(fit, 'lambda_0')$lambda_0, 2, mean)[2],
                                 obs3_lambda_0 = apply(rstan::extract(fit, 'lambda_0')$lambda_0, 2, mean)[3],
                                 obs1_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[1, 1],
                                 obs1_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[1, 2],
                                 obs1_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[1, 3],
                                 obs2_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[2, 1],
                                 obs2_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[2, 2],
                                 obs2_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[2, 3],
                                 obs3_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[3, 1],
                                 obs3_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[3, 2],
                                 obs3_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, c(2, 3), mean)[3, 3]
                                 )
                      )
    } else {
      output <- cbind(output,
                      data.frame(obs1_tau_0 = mean(rstan::extract(fit, 'tau_0')$tau_0),
                                 obs1_lambda_0 = mean(rstan::extract(fit, 'lambda_0')$lambda_0),
                                 obs1_gamma_0 = apply(rstan::extract(fit, 'gamma')$gamma, 2, mean)[1],
                                 obs1_gamma_1 = apply(rstan::extract(fit, 'gamma')$gamma, 2, mean)[2],
                                 obs1_gamma_2 = apply(rstan::extract(fit, 'gamma')$gamma, 2, mean)[3]
                                 )
                      )
      }
    }
  if(any(fit@model_pars == "overdispersion")) {
    par_name <- c(par_name, "overdispersion")
    output$overdispersion <- mean(rstan::extract(fit, 'overdispersion')$overdispersion)
    } 
  ### check convergence
  rhat <- do.call('c', lapply(par_name, check_conv, fit = fit))
  output$looic <- loo::loo(loo::extract_log_lik(fit))$estimates[3, 1]
  if(any(rhat > 1.05)) {
    output[1, ] <- rep(NA, ncol(output))
  }
  return(output)
}

get_results <- function(l, j, m) {
  load(paste("res/stan_data_", l, "_n", j, "_", m, ".RData", sep = ""))
  temp <- do.call('rbind', 
                  lapply(get(paste("poisson", m, sep = "_")), 
                         get_pointestimate
                         )
                  )
  par_name <- names(temp)
  temp %>% 
    mutate(sim = 1:n(),
           sample_size = j
           ) %>%
    pivot_longer(cols = all_of(par_name),
                 names_to = "param",
                 values_to = "estimate"
                 ) %>% 
    mutate(model = paste("poisson", m, sep = "_"),
           truth = l
           ) %>% 
    as.data.frame() %>%
    write.table(file = paste("res/results_", l, "_", j, "_", m, "_poisson.txt", sep = ""),
                quote = FALSE, col.names = TRUE, row.names = FALSE, sep = "\t"
                )
  ### negbin
  temp <- do.call('rbind', 
                  lapply(get(paste("negbin", m, sep = "_")), 
                         get_pointestimate
                         )
                  )
  par_name <- names(temp)
  temp %>% 
    mutate(sim = 1:n(),
           sample_size = j
           ) %>%
    pivot_longer(cols = all_of(par_name),
                 names_to = "param",
                 values_to = "estimate"
                 ) %>% 
    mutate(model = paste("negbin", m, sep = "_"),
           truth = l
           ) %>% 
    as.data.frame() %>%
    write.table(file = paste("res/results_", l, "_", j, "_", m, "_negbin.txt", sep = ""),
                quote = FALSE, col.names = TRUE, row.names = FALSE, sep = "\t"
                )
}

for(l in distrib) {
  for(j in n_obs) {
    for(m in model) {
      get_results(l = l, j = j, m = m)
    }
  }
}
